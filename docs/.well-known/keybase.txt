==================================================================
https://keybase.io/samuelchampagne
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://samuelchampagne.com
  * I am samuelchampagne (https://keybase.io/samuelchampagne) on keybase.
  * I have a public key with fingerprint F81D 602F EA5B 7FD4 AE89  78FA D556 03F4 F961 5A4F

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101b3bf51697602f335868a993c8551de08753a529fb403faf8ceae4b03e48bdf7f0a",
      "fingerprint": "f81d602fea5b7fd4ae8978fad55603f4f9615a4f",
      "host": "keybase.io",
      "key_id": "d55603f4f9615a4f",
      "kid": "0101b3bf51697602f335868a993c8551de08753a529fb403faf8ceae4b03e48bdf7f0a",
      "uid": "db7c11bcbf6abc7e2318f5ea1c782f00",
      "username": "samuelchampagne"
    },
    "merkle_root": {
      "ctime": 1525402370,
      "hash_meta": "c15950baf3b3338918746e912eb01cf72697b114a8e8c52a1f978c59b068aace",
      "seqno": 2478701
    },
    "revoke": {
      "sig_ids": [
        "ad56240c25a1c8d3018e6139878a317f1d95ce998698f6ed6487bfea95661fe30f"
      ]
    },
    "service": {
      "hostname": "samuelchampagne.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1525402384,
  "expire_in": 157680000,
  "prev": "32b7ec39737ff2f5cb1396282f43de27c8fc3bf10a65784eb8b70a566436db3c",
  "seqno": 39,
  "tag": "signature"
}

which yields the signature:

-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.76
Comment: https://keybase.io/crypto

yMK8AnicrVNfiFVFHL6rruHm6mYiPZSbJymUdZk5c+bMnEvCSstSSNQSIZvFZWbO
b+493HvPufecs9eW/YdgpGuYSG7EBoYpIpELbmz7KGtlcfHBB5EyqLDA2Cyt3AgJ
mrMVQfjYvAwzfL/v933fb+aj9uW5tpZDc0/Nnz26VrY05/cN5l5YaHYMWzLyh6z8
sFWGpQ0qPiRpoRz4Vt5CGGFJpKbY9ZiLbE0I5S4XnkcUpxT7gDijRFDb09JBRAvN
FQhwJCLgcOlrppGwuiwdhEWIa3EQpoZWc+xnbCCoZNp3BHCPcS18Sl1D4mjPxVQ4
2hSWoiSrMOKkSKA7iMydORSW5N0F/z/rHvyrj2QKY6mkdoVUDGyCuaYgsGLc1ghl
wATiUFTBoBNRHYSKKolqTRRDsEa7rCrE5QoU4ihKs4xVGmRITG3qIJswZHyKpFSo
QioMgcLUo0gKTSQhhHuYM8cFD9sgEVaa2caUxNgRHLiitsDapKeoJ5GxKBQYOQnU
w8jK2w7jDGGjIIZGVIaseRIUTXqJld9tmcBd20HKpsYK9wnCHFxMPM64IJhp7HtU
gedx1+PaBd91OJNmah51XayBIG29NJo1ixuBWiLPxnX3GLpVVDXCanGURiqqGEAp
TWtJPosnHaplFXtAFv7mKsgg9M2bMRUNiJMgCk1cBvmf5LjTZcHLtSCGQpAhKHM5
MivrAw1DSWzJQBGPEaa1ramSxp5rm6k5xAebKa6VeScYCZcy7oDkkiFh7DnE9SVR
/0ZJPKNTFDNjQTEU6WBsJjt/7sUVuZa23MrWZdlnyrWt6vjni12fuedO60X97pWF
Bdg7tfGtFSvHx87c3ld/re9gb6/6eufk8R0HTp/+YNXYtR9+/O3wwd7zY9NH2c1z
26v7k9z0yRMfb3l/69kLX3zTeaHr9aHHrEs99w1su7il2d830Hf1Qbxj4tM1Z3b2
z9zgu+t9t1aPt+efyH3+/DtRY2PzRhGP1788tvnet7c+VJx849nFE5tWV+eCI7+f
b52+ZG1AnzR3Heu5/84j69a+eXn+vZHZwrKRn5/+ZWpi/frvvq/Xtw8cKs/619eM
3Bo6UHp1/4bJP5771t/1QOe1w6d65wbKi/7xzuFfn5zKB3se75npXnxm+cTDtz/c
9NnJq+P5cPbRnzYvvLL3SP/NSvtUBx/9avRPVJiGUw==
=zcLT
-----END PGP MESSAGE-----

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/samuelchampagne

==================================================================
