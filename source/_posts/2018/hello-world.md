---
title: Hello World
date: 2018-05-02 19:45:20
license: 'https://creativecommons.org/licenses/by/4.0/'
search_hidden: true
---

Welcome to [Hexo](https://hexo.io/)! This is your first post. Check [documentation](https://hexo.io/docs/) for more info. If you get any problems when using Hexo, you can find the answer in [troubleshooting](https://hexo.io/docs/troubleshooting.html) or you can ask me on [GitHub](https://github.com/hexojs/hexo/issues).

## Quick Start

### Create a new post

``` bash
$ hexo new draft "My New Post"
$ atom source/_drafts/2017/my-new-post.md
$ hexo publish draft "My New Post"
$ hexo server
```

More info: [Writing](https://hexo.io/docs/writing.html)

``` javascript
function foo(a, b) {
  return a + b;
}
```

Or alternatively

``` javascript
const foo = (a, b) => a + b
```

> Lorem ipsum dolor sit amet, consectetur adipisicing elit, `sed do eiusmod tempor` incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

---

### Run server

- [x] Install hexo
- [ ] Run Hexo

``` bash
$ hexo server
```

More info: [Server](https://hexo.io/docs/server.html)

### Generate static files

``` bash
$ hexo generate
```

More info: [Generating](https://hexo.io/docs/generating.html)

### Deploy to remote sites

``` bash
$ hexo deploy
```

More info: [Deployment](https://hexo.io/docs/deployment.html)
